# author: "Jean-Etienne Morlighem <jemorlighem@vegetalsignals.com>"
# date: 2023-01


if __name__ == "__main__":
    from model import model
else:
    from .model import model


def compute(compute_input):

    data = compute_input["data_list"]

    try:
        compute_output = {
            "output": {
                "feed_length": model.feed_length(data), 
                "feed_sum": model.feed_sum(data), 
                "feed_average": model.feed_average(data), 
                "feed_median": model.feed_median(data), 
                "feed_stdev": model.feed_stdev(data), 
                }, 
            "output_issue_status": 0, 
            }

    except:
        compute_output = {
            "output": {}, 
            "output_issue_status": 1, 
            }

    return compute_output

